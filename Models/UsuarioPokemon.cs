﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class UsuarioPokemon
    {
        public int Id { get; set; }
        public int IDUSUARIO { get; set; }
        public int IDPOKEMON { get; set; }
        public int CANTIDAD { get; set; }
        public DateTime FECHA { get; set; }
        public Pokemon Pokemon { get; set; }
    }
}
