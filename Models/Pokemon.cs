﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Pokemon
    {
        public int IDPOKEMON { get; set; }
        public string NOMBRE { get; set; }
        public string TIPO { get; set; }
        public string IMAGEN { get; set; }
        public List<UsuarioPokemon> UsuarioPokemon { get; set; }
    }
}
