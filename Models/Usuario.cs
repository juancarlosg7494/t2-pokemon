﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Usuario
    {
        public int IDUSUARIO { get; set; }
        public string NOMBRE { get; set; }
        public string CORREO { get; set; }
        public string CONTRASEÑA { get; set; }
    }
}
