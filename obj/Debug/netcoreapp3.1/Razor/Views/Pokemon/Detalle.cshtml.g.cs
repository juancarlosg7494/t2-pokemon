#pragma checksum "C:\Users\javie\Desktop\T2\WebApplication1\Views\Pokemon\Detalle.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "3063cfa127955d0149aeeddd5c93c6ba478c9bf1"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Pokemon_Detalle), @"mvc.1.0.view", @"/Views/Pokemon/Detalle.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\javie\Desktop\T2\WebApplication1\Views\_ViewImports.cshtml"
using WebApplication1;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\javie\Desktop\T2\WebApplication1\Views\_ViewImports.cshtml"
using WebApplication1.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3063cfa127955d0149aeeddd5c93c6ba478c9bf1", @"/Views/Pokemon/Detalle.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"729efaa87342638aecfe1a972ce9f9f8dff55b4c", @"/Views/_ViewImports.cshtml")]
    public class Views_Pokemon_Detalle : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("<h1>\r\n    ");
#nullable restore
#line 2 "C:\Users\javie\Desktop\T2\WebApplication1\Views\Pokemon\Detalle.cshtml"
Write(Model.Pokemon.NOMBRE);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n</h1>\r\n\r\n<h3>Pokemon -> Detalle</h3>\r\n");
#nullable restore
#line 6 "C:\Users\javie\Desktop\T2\WebApplication1\Views\Pokemon\Detalle.cshtml"
 foreach (var item in Model.Pokemon)
{

#line default
#line hidden
#nullable disable
            WriteLiteral("    <div class=\"card mb-2\">\r\n        <div class=\"card-body\">\r\n            <h5 class=\"card-title\">item.username</h5>\r\n            <a");
            BeginWriteAttribute("href", " href=\"", 243, "\"", 300, 2);
            WriteAttributeValue("", 250, "https://localhost:44346/pokemon/listar?id=", 250, 42, true);
#nullable restore
#line 11 "C:\Users\javie\Desktop\T2\WebApplication1\Views\Pokemon\Detalle.cshtml"
WriteAttributeValue("", 292, item.Id, 292, 8, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" class=\"card-link\">");
#nullable restore
#line 11 "C:\Users\javie\Desktop\T2\WebApplication1\Views\Pokemon\Detalle.cshtml"
                                                                                      Write(item.Dislikes);

#line default
#line hidden
#nullable disable
            WriteLiteral(" Dislike</a>\r\n        </div>\r\n    </div>\r\n");
#nullable restore
#line 14 "C:\Users\javie\Desktop\T2\WebApplication1\Views\Pokemon\Detalle.cshtml"

}

#line default
#line hidden
#nullable disable
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
