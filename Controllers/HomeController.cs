﻿using WebApplication1.DB;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private AppPokemonContext context;

        public HomeController(AppPokemonContext context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            var pokemon = context.Pokemons.Include("UsuarioPokemon.Pokemon").ToList();
            return View("Index", pokemon);
        }
    }
}
