﻿using WebApplication1.DB.Mapping;
using WebApplication1.Models;
using Microsoft.EntityFrameworkCore;

namespace WebApplication1.DB
{
    public class AppPokemonContext : DbContext
    {
        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<Pokemon> Pokemons { get; set; }

        public AppPokemonContext(DbContextOptions<AppPokemonContext> options): base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new UsuarioMap());
            modelBuilder.ApplyConfiguration(new PokemonMap());
            modelBuilder.ApplyConfiguration(new UsuarioPokemonMap());
        }
    }
}
