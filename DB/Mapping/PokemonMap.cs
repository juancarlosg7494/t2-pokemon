﻿using WebApplication1.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WebApplication1.DB.Mapping
{
    public class PokemonMap : IEntityTypeConfiguration<Pokemon>
    {
        public void Configure(EntityTypeBuilder<Pokemon> builder)
        {
            builder.ToTable("POKEMON");
            builder.HasKey(pok => pok.IDPOKEMON);

            builder.HasMany(o => o.UsuarioPokemon)
                .WithOne()
                .HasForeignKey(o => o.IDPOKEMON);
        }
    }
}
