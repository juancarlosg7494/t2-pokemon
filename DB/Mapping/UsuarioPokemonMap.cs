﻿using WebApplication1.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WebApplication1.DB.Mapping
{
    public class UsuarioPokemonMap : IEntityTypeConfiguration<UsuarioPokemon>
    {
        public void Configure(EntityTypeBuilder<UsuarioPokemon> builder)
        {
            builder.ToTable("USUARIOPOKEMON");
            builder.HasKey(o => o.Id);

            builder.HasOne(o => o.Pokemon)
                .WithMany()
                .HasForeignKey(o => o.IDUSUARIO);
        }
    }
}
